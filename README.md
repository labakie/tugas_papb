# Tugas 1 & Tugas 2 PAPB

## Description

Repository for the tasks of mobile application programming via Android Studio. The main goal of the first task is to make a biodata that contains my name, NIM, and hobby. Furthermore, the second task is the upgraded version of the first, with a picture, button, and landscape version added. Both tasks are located within the same repository but in different activity layouts.

## Screenshot

<table>
    <tr>
        <td style="text-align: center">Tugas 1</td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/labakie/tugas_papb/uploads/44aefaa4c68913cc657d5c1a3f802946/tugas_01_revisi.jpg" width="240" height="427"></td>
    </tr>
</table>

<table>
    <tr>
        <td style="text-align: center">Tugas 2-portrait</td>
        <td style="text-align: center">Tugas 2-landscape</td>
    </tr>
    <tr>
        <td><img src="https://gitlab.com/labakie/tugas_papb/uploads/99885de7dbbd5eb2cfe1c157fe044cba/tugas_02_portrait.jpg" width="240" height="427"></td>
        <td><img src="https://gitlab.com/labakie/tugas_papb/uploads/ecd1c2f2e9f1fef7d141569100abebf1/tugas_02_landscape.jpg" width="427" height="240"></td>
    </tr>
</table>
